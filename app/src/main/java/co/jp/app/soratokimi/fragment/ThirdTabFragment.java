package co.jp.app.soratokimi.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Nguyen Cong Tuyen on 5/30/2016.
 */
public class ThirdTabFragment extends BaseFragment {
    public static final int TAB_INDEX = 2;
    @Override
    protected View onCreateViewLayout(LayoutInflater pInflater, ViewGroup pContainer, Bundle pSavedInstanceState) {
        return null;
    }
}
