package co.jp.app.soratokimi.utils;

/**
 * Created by Nguyen Cong Tuyen on 5/31/2016.
 */
public class TimeUtils {
    private static String[] sDayOfWeek = new String[]{"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
    public static String getDayOffWeekString(int pDayOffWeek){
        return sDayOfWeek[pDayOffWeek];
    }
}
