package co.jp.app.soratokimi.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.jp.app.soratokimi.R;
import co.jp.app.soratokimi.adapter.ViewPagerAdapter;

/**
 * Created by Nguyen Cong Tuyen on 5/30/2016.
 */
public class MainMenuFragment extends BaseFragment implements TabLayout.OnTabSelectedListener {
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private ViewPagerAdapter mViewPagerAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsShowHeader = false;
    }

    @Override
    protected View onCreateViewLayout(LayoutInflater pInflater, ViewGroup pContainer, Bundle pSavedInstanceState) {
        View rootView = pInflater.inflate(R.layout.fragment_main_menu, pContainer, false);
        mViewPager = (ViewPager) rootView.findViewById(R.id.vp_pager);
        mTabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mViewPagerAdapter);

        initTabLayout();
        return rootView;
    }

    private void initTabLayout() {
        TabLayout.Tab tab = mTabLayout.newTab();
        View view = new View(getContext());
        view.setBackgroundResource(R.drawable.bg_tab_time_selector);
        tab.setCustomView(view);
        mTabLayout.addTab(tab);

        tab = mTabLayout.newTab();
        view = new View(getContext());
        view.setBackgroundResource(R.drawable.bg_tab_location_selector);
        tab.setCustomView(view);
        mTabLayout.addTab(tab);

        tab = mTabLayout.newTab();
        view = new View(getContext());
        view.setBackgroundResource(R.drawable.bg_tab_search_selector);
        tab.setCustomView(view);
        mTabLayout.addTab(tab);

        tab = mTabLayout.newTab();
        view = new View(getContext());
        view.setBackgroundResource(R.drawable.bg_tab_book_selector);
        tab.setCustomView(view);
        mTabLayout.addTab(tab);

        tab = mTabLayout.newTab();
        view = new View(getContext());
        view.setBackgroundResource(R.drawable.bg_tab_heart_selector);
        tab.setCustomView(view);
        mTabLayout.addTab(tab);
        mTabLayout.setOnTabSelectedListener(this);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public boolean onBackPressed() {
        BaseFragment fragment = (BaseFragment) mViewPagerAdapter.getFragmentItem(mViewPager
                .getCurrentItem());

        if (fragment != null) {
            return fragment.onBackPressed();
        } else {
            return false;
        }
    }
}
