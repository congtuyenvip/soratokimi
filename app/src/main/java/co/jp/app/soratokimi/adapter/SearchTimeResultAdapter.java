package co.jp.app.soratokimi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.jp.app.soratokimi.R;
import co.jp.app.soratokimi.data.SearchTimeResult;

/**
 * Created by Nguyen Cong Tuyen on 5/31/2016.
 */
public class SearchTimeResultAdapter extends RecyclerView.Adapter {

    private List<SearchTimeResult> mSearchRecords;
    private static OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public SearchTimeResultAdapter(List<SearchTimeResult> pSearchRecords) {
        mSearchRecords = pSearchRecords;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_search_time_result, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder viewHolder = (ItemViewHolder) holder;
        SearchTimeResult searchTimeResult = mSearchRecords.get(position);
        viewHolder.mNameView.setText(searchTimeResult.getName());
        viewHolder.mTypeView.setText(searchTimeResult.getType());
        viewHolder.mDistanceView.setText(searchTimeResult.getDistance());
        viewHolder.mImageView.setImageResource(R.drawable.bg_time_search_1);
    }

    @Override
    public int getItemCount() {
        return mSearchRecords == null ? 0 : mSearchRecords.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageView;
        private TextView mTypeView, mDistanceView, mNameView;

        public ItemViewHolder(final View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.iv_item_image);
            mTypeView = (TextView) itemView.findViewById(R.id.tv_type);
            mDistanceView = (TextView) itemView.findViewById(R.id.tv_distance);
            mNameView = (TextView) itemView.findViewById(R.id.tv_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null)
                        mListener.onItemClick(itemView, getLayoutPosition());
                }
            });
        }
    }
}
