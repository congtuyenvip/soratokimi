package co.jp.app.soratokimi.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import co.jp.app.soratokimi.R;

/**
 * Created by Nguyen Cong Tuyen on 5/30/2016.
 */
public class FirstTabFragment extends BaseFragment implements View.OnClickListener {
    public static final int TAB_INDEX = 0;
    Button mHairSalonSearchView;
    Button mRelaxionSalonSearchView;
    Button mBeautySalonSearchView;
    Button mNailEyeSalonSearchView;
    @Override
    protected View onCreateViewLayout(LayoutInflater pInflater, ViewGroup pContainer, Bundle pSavedInstanceState) {
        View rootView = pInflater.inflate(R.layout.fragment_tab_time, pContainer, false);
        initView(rootView);
        setupView();
        return rootView;
    }

    private void setupView() {
        mHairSalonSearchView.setOnClickListener(this);
        mRelaxionSalonSearchView.setOnClickListener(this);
        mBeautySalonSearchView.setOnClickListener(this);
        mNailEyeSalonSearchView.setOnClickListener(this);
    }


    private void initView(View pRootView) {
        mHairSalonSearchView = (Button) pRootView.findViewById(R.id.ib_search_hair_salon);
        mRelaxionSalonSearchView = (Button) pRootView.findViewById(R.id.ib_search_relaxation_salon);
        mBeautySalonSearchView = (Button) pRootView.findViewById(R.id.ib_search_beauty_salon);
        mNailEyeSalonSearchView = (Button) pRootView.findViewById(R.id.ib_search_nail_eyelashes_salon);

    }

    @Override
    public void onClick(View v) {
        nextFragment(SearchTimeResultFragment.class.getName(), null);
    }
}
