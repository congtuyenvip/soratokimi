package co.jp.app.soratokimi.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import co.jp.app.soratokimi.R;

/**
 * Created by Nguyen Cong Tuyen on 5/27/2016.
 */

public class FragmentHeaderView extends LinearLayout implements View.OnClickListener{

    private ImageView mMenuView, mSearchView;
    private HeaderEventListener mHeaderEventListener;

    public FragmentHeaderView(Context context) {
        super(context);
        init();
    }
    public FragmentHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FragmentHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FragmentHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LinearLayout viewRoot = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.fragment_header,this, false);
        mMenuView = (ImageView) viewRoot.findViewById(R.id.iv_header_menu);
        mSearchView = (ImageView) viewRoot.findViewById(R.id.iv_header_option_search);
        mMenuView.setOnClickListener(this);
        mSearchView.setOnClickListener(this);
        addView(viewRoot, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    }

    public HeaderEventListener getHeaderEventListener() {
        return mHeaderEventListener;
    }

    public void setHeaderEventListener(HeaderEventListener pHeaderEventListener) {
        mHeaderEventListener = pHeaderEventListener;
    }

    @Override
    public void onClick(View v) {
        if(mHeaderEventListener == null)
            return;
        if(v.getId() == R.id.iv_header_menu){
            mHeaderEventListener.onMenuClickEvent();
        }
        else if(v.getId() == R.id.iv_header_option_search){
            mHeaderEventListener.onOptionItemClickEvent();
        }
    }


    public static interface HeaderEventListener {
        void onMenuClickEvent();

        void onOptionItemClickEvent();
    }
}
