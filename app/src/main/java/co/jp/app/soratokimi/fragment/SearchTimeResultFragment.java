package co.jp.app.soratokimi.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import co.jp.app.soratokimi.R;
import co.jp.app.soratokimi.adapter.SearchTimeResultAdapter;
import co.jp.app.soratokimi.data.SearchTimeResult;
import co.jp.app.soratokimi.utils.TimeUtils;
import co.jp.app.soratokimi.adapter.TimeSelectionAdapter;
import co.jp.app.soratokimi.fragment.BaseFragment;
import co.jp.app.soratokimi.view.DateView;
import co.jp.app.soratokimi.view.DividerItemDecoration;

/**
 * Created by Nguyen Cong Tuyen on 5/31/2016.
 */
public class SearchTimeResultFragment extends BaseFragment {


    private DateView mDateView;
    private TextView mDateOfWeekView, mTime;
    private ListView mTimeSlotView;
    private List<String> mTimeSlots;
    private List<SearchTimeResult> mSearchRecords;
    private RecyclerView mSearchResultView;
    private SearchTimeResultAdapter mSearchResultAdapter;
    private ImageView mBackView;

    @Override
    protected View onCreateViewLayout(LayoutInflater pInflater, ViewGroup pContainer, Bundle pSavedInstanceState) {
        View rootView = pInflater.inflate(R.layout.fragment_time_search_result, pContainer, false);
        initView(rootView);
        setupView();
        return rootView;
    }

    private void setupView() {
        Calendar calendar = Calendar.getInstance();
        mDateView.setDate(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        mDateView.setMonth(String.valueOf(calendar.get(Calendar.MONTH) + 1));
        mDateView.invalidate();
        mDateOfWeekView.setText(TimeUtils.getDayOffWeekString(calendar.get(Calendar.DAY_OF_WEEK) - 1));

        mSearchRecords = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SearchTimeResult item = new SearchTimeResult();
            item.setDistance("ヘアサロン: " + i + "km");
            item.setType("ヘアサロン");
            item.setName("ヘアサロン " + i);
            mSearchRecords.add(item);
        }
        mSearchResultAdapter = new SearchTimeResultAdapter(mSearchRecords);
        mSearchResultView.setAdapter(mSearchResultAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mSearchResultView.setLayoutManager(layoutManager);
        mSearchResultView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST));

        mTimeSlots = new ArrayList<>();
        for (int i = 8; i < 24; i++) {
            mTimeSlots.add(String.format("%2d:00", i));
        }
        TimeSelectionAdapter timeSelectionAdapter = new TimeSelectionAdapter(mTimeSlots);
        mTimeSlotView.setAdapter(timeSelectionAdapter);
        mTimeSlotView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDateOfWeekView.setVisibility(View.GONE);
                mDateView.setVisibility(View.GONE);
                mTime.setText(mTimeSlots.get(position));
//                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_left);
//                animation.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        mTimeSlotView.setVisibility(View.GONE);
//                        mSearchResultView.setVisibility(View.VISIBLE);
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });
//                mTimeSlotView.startAnimation(animation);

                mTimeSlotView.setVisibility(View.GONE);
                mSearchResultView.setVisibility(View.VISIBLE);
            }
        });

        mBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTimeSlotView.getVisibility() == View.VISIBLE){
                    getActivity().onBackPressed();
                }
                else {
                    mTimeSlotView.setVisibility(View.VISIBLE);
                    mSearchResultView.setVisibility(View.GONE);
                }
            }
        });
    }


    private void initView(View pRootView) {
        mDateView = (DateView) pRootView.findViewById(R.id.v_date_view);
        mDateOfWeekView = (TextView) pRootView.findViewById(R.id.tv_day_off_week);
        mTime = (TextView) pRootView.findViewById(R.id.tv_time);
        mTimeSlotView = (ListView) pRootView.findViewById(R.id.lv_time_slot);
        mSearchResultView = (RecyclerView) pRootView.findViewById(R.id.rv_search_result);
        mBackView = (ImageView) pRootView.findViewById(R.id.iv_back);
    }
}
