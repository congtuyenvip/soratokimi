package co.jp.app.soratokimi.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import co.jp.app.soratokimi.R;

/**
 * Created by Nguyen Cong Tuyen on 5/31/2016.
 */
public class TimeSelectionAdapter extends BaseAdapter {
    private List<String> mTimeSlot;

    public TimeSelectionAdapter(List<String> pTimeSlot) {
        mTimeSlot = pTimeSlot;
    }

    @Override
    public int getCount() {
        return mTimeSlot == null ? 0 : mTimeSlot.size();
    }

    @Override
    public Object getItem(int position) {
        return mTimeSlot == null ? null : mTimeSlot.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mTimeSlot == null ? -1 : position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time_view, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.mTimeTextView = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (mTimeSlot == null || mTimeSlot.size() <= position)
            return convertView;
        viewHolder.mTimeTextView.setText(mTimeSlot.get(position));
        return convertView;
    }

    private static class ViewHolder {
        TextView mTimeTextView;
    }
}
