package co.jp.app.soratokimi.data;

/**
 * Created by Nguyen Cong Tuyen on 5/31/2016.
 */
public class SearchTimeResult {
    private String mImageUrl;
    private String mType;
    private String mDistance;
    private String mName;

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String pImageUrl) {
        mImageUrl = pImageUrl;
    }

    public String getType() {
        return mType;
    }

    public void setType(String pType) {
        mType = pType;
    }

    public String getDistance() {
        return mDistance;
    }

    public void setDistance(String pDistance) {
        mDistance = pDistance;
    }

    public String getName() {
        return mName;
    }

    public void setName(String pName) {
        mName = pName;
    }
}
