package co.jp.app.soratokimi.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Nguyen Cong Tuyen on 5/30/2016.
 */
public class DateView extends View {

    private String mDate;
    private String mMonth;
    private TextPaint mPaint;
    private Rect mTextBound;

    public DateView(Context context) {
        super(context);
        init();
    }

    public DateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DateView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mPaint = new TextPaint();
        mPaint.setColor(Color.WHITE);
        mPaint.setAntiAlias(true);
        mPaint.setTextSize(86);
        mTextBound = new Rect();
        mPaint.getTextBounds("00", 0, 2, mTextBound);
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String pDate) {
        mDate = pDate;
    }

    public String getMonth() {
        return mMonth;
    }

    public void setMonth(String pMonth) {
        mMonth = pMonth;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mDate == null || mMonth == null)
            return;
        canvas.save();
        canvas.drawText(mDate, (getWidth() / 2 - mTextBound.width()) / 2, (getHeight() / 2 - mTextBound.height()) / 2 - mTextBound.top, mPaint);
        canvas.drawText(mMonth, getWidth() / 2 + (getWidth() / 2 - mTextBound.width()) / 2, canvas.getHeight() / 2 + (getHeight() / 2 - mTextBound.height()) / 2 - mTextBound.top, mPaint);
        mPaint.setStrokeWidth(8);
        canvas.drawLine(0,getHeight(),getWidth(),0, mPaint);
    }
}
