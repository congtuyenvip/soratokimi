package co.jp.app.soratokimi.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import co.jp.app.soratokimi.MainActivity;
import co.jp.app.soratokimi.R;
import co.jp.app.soratokimi.view.FragmentHeaderView;

/**
 * Created by Nguyen Cong Tuyen on 5/27/2016.
 */
public abstract class BaseFragment extends Fragment implements FragmentHeaderView.HeaderEventListener {
    private FragmentHeaderView mFragmentHeaderView;
    protected boolean mIsShowHeader = true;
    private FragmentHeaderView.HeaderEventListener mHeaderEventListener;

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_base, container, false);
        mFragmentHeaderView = (FragmentHeaderView) rootView.findViewById(R.id.view_fragment_header);
        mFragmentHeaderView.setVisibility(mIsShowHeader ? View.VISIBLE : View.GONE);
        mFragmentHeaderView.setHeaderEventListener(this);
        View contentView = onCreateViewLayout(inflater, container, savedInstanceState);
        if (contentView != null) {
            LinearLayout contentContainer = (LinearLayout) rootView.findViewById(R.id.layout_fragment_main_container);
            contentContainer.addView(contentView);
        }
        return rootView;
    }

    protected abstract View onCreateViewLayout(LayoutInflater pInflater, ViewGroup pContainer, Bundle pSavedInstanceState);

    public void setShowHeader(boolean pShowHeader) {
        mIsShowHeader = pShowHeader;
        if (mFragmentHeaderView != null)
            mFragmentHeaderView.setVisibility(mIsShowHeader ? View.VISIBLE : View.GONE);
    }

    public FragmentHeaderView.HeaderEventListener getHeaderEventListener() {
        return mHeaderEventListener;
    }

    public void setHeaderEventListener(FragmentHeaderView.HeaderEventListener pHeaderEventListener) {
        mHeaderEventListener = pHeaderEventListener;
    }

    @Override
    public void onMenuClickEvent() {
        if (getActivity() == null)
            return;
        MainActivity activity = (MainActivity) getActivity();
        activity.openMenu();
    }

    @Override
    public void onOptionItemClickEvent() {
        if (mHeaderEventListener == null)
            return;
        mHeaderEventListener.onOptionItemClickEvent();
    }

    public Fragment nextFragment(String pClassName, Bundle pAgruments) {
        if (pClassName == null || pClassName.length() <= 0) {
            return null;
        }
        try {
            Object objFragment = Class.forName(pClassName).getConstructor().newInstance();
            Fragment fragment = null;
            if (objFragment instanceof Fragment) {
                fragment = (Fragment) objFragment;
            }
            fragment.setArguments(pAgruments);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

            transaction.addToBackStack(pClassName);
            transaction.replace(R.id.layout_fragment_base_root, fragment, pClassName).commit();

            return fragment;
        } catch (Exception pE) {
        }
        return null;
    }

    public boolean onBackPressed(){
        int childCount = getChildFragmentManager().getBackStackEntryCount();
        if(childCount == 0){
            return false;
        }
        else{
            FragmentManager childFragmentManager = getChildFragmentManager();

            FragmentManager.BackStackEntry backEntry = childFragmentManager.getBackStackEntryAt(childCount - 1);
            String str = backEntry.getName();
            BaseFragment childFragment = (BaseFragment) childFragmentManager.findFragmentByTag(str);
            if(childFragment != null){
                if(!childFragment.onBackPressed()){
                    childFragmentManager.popBackStackImmediate();
                }
            }
            else {
                childFragmentManager.popBackStackImmediate();
            }
            return true;
        }
    }
}
