package co.jp.app.soratokimi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import co.jp.app.soratokimi.fragment.BaseFragment;
import co.jp.app.soratokimi.fragment.FifthTabFragment;
import co.jp.app.soratokimi.fragment.FirstTabFragment;
import co.jp.app.soratokimi.fragment.FourthTabFragment;
import co.jp.app.soratokimi.fragment.SecondTabFragment;
import co.jp.app.soratokimi.fragment.ThirdTabFragment;

/**
 * Created by Nguyen Cong Tuyen on 5/30/2016.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    private SparseArray<BaseFragment> mFragments = new SparseArray<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case FirstTabFragment.TAB_INDEX:
                fragment = new FirstTabFragment();
                break;
            case SecondTabFragment.TAB_INDEX:
                fragment = new SecondTabFragment();
                break;
            case ThirdTabFragment.TAB_INDEX:
                fragment = new ThirdTabFragment();
                break;
            case FourthTabFragment.TAB_INDEX:
                fragment = new FourthTabFragment();
                break;
            case FifthTabFragment.TAB_INDEX:
                fragment = new FifthTabFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 5;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        mFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        BaseFragment fragment = (BaseFragment) super.instantiateItem(container, position);
        mFragments.put(position, fragment);
        return fragment;
    }
    public Fragment getFragmentItem(int pCurrentItem) {
        return mFragments.get(pCurrentItem) != null ? mFragments.get(pCurrentItem) : null;
    }
}
